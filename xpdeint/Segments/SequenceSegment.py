#!/usr/bin/env python3




##################################################
## DEPENDENCIES
import sys
import os
import os.path
try:
    import builtins as builtin
except ImportError:
    import __builtin__ as builtin
from os.path import getmtime, exists
import time
import types
from Cheetah.Version import MinCompatibleVersion as RequiredCheetahVersion
from Cheetah.Version import MinCompatibleVersionTuple as RequiredCheetahVersionTuple
from Cheetah.Template import Template
from Cheetah.DummyTransaction import *
from Cheetah.NameMapper import NotFound, valueForName, valueFromSearchList, valueFromFrameOrSearchList
from Cheetah.CacheRegion import CacheRegion
import Cheetah.Filters as Filters
import Cheetah.ErrorCatchers as ErrorCatchers
from Cheetah.compat import unicode
from xpdeint.Segments._Segment import _Segment
from xpdeint.CallOnceGuards import callOncePerInstanceGuard

##################################################
## MODULE CONSTANTS
VFFSL=valueFromFrameOrSearchList
VFSL=valueFromSearchList
VFN=valueForName
currentTime=time.time
__CHEETAH_version__ = '3.2.6.post2'
__CHEETAH_versionTuple__ = (3, 2, 6, 'post', 2)
__CHEETAH_genTime__ = 1634954793.0467293
__CHEETAH_genTimestamp__ = 'Sat Oct 23 13:06:33 2021'
__CHEETAH_src__ = '/home/mattias/xmds-3.0.0/admin/staging/xmds-3.1.0/xpdeint/Segments/SequenceSegment.tmpl'
__CHEETAH_srcLastModified__ = 'Thu Apr  4 16:29:24 2019'
__CHEETAH_docstring__ = 'Autogenerated by Cheetah: The Python-Powered Template Engine'

if __CHEETAH_versionTuple__ < RequiredCheetahVersionTuple:
    raise AssertionError(
      'This template was compiled with Cheetah version'
      ' %s. Templates compiled before version %s must be recompiled.'%(
         __CHEETAH_version__, RequiredCheetahVersion))

##################################################
## CLASSES

class SequenceSegment(_Segment):

    ##################################################
    ## CHEETAH GENERATED METHODS


    def __init__(self, *args, **KWs):

        super(SequenceSegment, self).__init__(*args, **KWs)
        if not self._CHEETAH__instanceInitialized:
            cheetahKWArgs = {}
            allowedKWs = 'searchList namespaces filter filtersLib errorCatcher'.split()
            for k,v in KWs.items():
                if k in allowedKWs: cheetahKWArgs[k] = v
            self._initCheetahInstance(**cheetahKWArgs)
        

    def description(self, **KWS):



        ## Generated from @def description: segment $segmentNumber (Sequence) at line 30, col 1.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        write('''segment ''')
        _v = VFFSL(SL,"segmentNumber",True) # '$segmentNumber' on line 30, col 27
        if _v is not None: write(_filter(_v, rawExpr='$segmentNumber')) # from line 30, col 27.
        write(''' (Sequence)''')
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def segmentFunctionBody(self, function, **KWS):



        ## CHEETAH: generated from @def segmentFunctionBody($function) at line 32, col 1.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        # 
        extraIndent = 0
        if VFFSL(SL,"localCycles",True) > 1: # generated from line 35, col 3
            write('''// Initialise child segments
''')
            _v = VFFSL(SL,"allocate",True) # '${allocate}' on line 37, col 1
            if _v is not None: write(_filter(_v, rawExpr='${allocate}')) # from line 37, col 1.
            _v = VFFSL(SL,"initialise",True) # '${initialise}' on line 38, col 1
            if _v is not None: write(_filter(_v, rawExpr='${initialise}')) # from line 38, col 1.
            write('''for (unsigned long _cycle = 0; _cycle < ''')
            _v = VFFSL(SL,"localCycles",True) # '${localCycles}' on line 39, col 41
            if _v is not None: write(_filter(_v, rawExpr='${localCycles}')) # from line 39, col 41.
            write('''; _cycle++) {
''')
            extraIndent = 2
        # 
        _v = VFFSL(SL,"callChildSegments",False)(function) # '${callChildSegments(function), extraIndent=extraIndent}' on line 43, col 1
        if _v is not None: write(_filter(_v, extraIndent=extraIndent, rawExpr='${callChildSegments(function), extraIndent=extraIndent}')) # from line 43, col 1.
        # 
        if VFFSL(SL,"localCycles",True) > 1: # generated from line 45, col 3
            write('''}
''')
            extraIndent = 0
            write('''// Finalise child segments
''')
            _v = VFFSL(SL,"finalise",True) # '${finalise}' on line 49, col 1
            if _v is not None: write(_filter(_v, rawExpr='${finalise}')) # from line 49, col 1.
            _v = VFFSL(SL,"free",True) # '${free}' on line 50, col 1
            if _v is not None: write(_filter(_v, rawExpr='${free}')) # from line 50, col 1.
        # 
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def callChildSegments(self, function, **KWS):



        ## CHEETAH: generated from @def callChildSegments($function) at line 55, col 1.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        # 
        for segment in VFFSL(SL,"childSegments",True): # generated from line 57, col 3
            _v = VFN(VFN(VFFSL(SL,"segment",True),"functions",True)['segment'],"call",False)(parentFunction=function) # "${segment.functions['segment'].call(parentFunction=function)}" on line 58, col 1
            if _v is not None: write(_filter(_v, rawExpr="${segment.functions['segment'].call(parentFunction=function)}")) # from line 58, col 1.
            write('''
''')
        # 
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    @callOncePerInstanceGuard
    def allocate(self, **KWS):



        ## CHEETAH: generated from @def allocate at line 64, col 1.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        # 
        _v = super(SequenceSegment, self).allocate()
        if _v is not None: write(_filter(_v))
        # 
        for segment in VFFSL(SL,"childSegments",True): # generated from line 68, col 3
            _v = VFFSL(SL,"segment.allocate",True) # '$segment.allocate' on line 69, col 1
            if _v is not None: write(_filter(_v, rawExpr='$segment.allocate')) # from line 69, col 1.
        # 
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    @callOncePerInstanceGuard
    def free(self, **KWS):



        ## CHEETAH: generated from @def free at line 75, col 1.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        # 
        _v = super(SequenceSegment, self).free()
        if _v is not None: write(_filter(_v))
        # 
        for segment in VFFSL(SL,"childSegments",True): # generated from line 79, col 3
            _v = VFFSL(SL,"segment.free",True) # '$segment.free' on line 80, col 1
            if _v is not None: write(_filter(_v, rawExpr='$segment.free')) # from line 80, col 1.
        # 
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    @callOncePerInstanceGuard
    def initialise(self, **KWS):



        ## CHEETAH: generated from @def initialise at line 86, col 1.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        # 
        _v = super(SequenceSegment, self).initialise()
        if _v is not None: write(_filter(_v))
        # 
        for segment in VFFSL(SL,"childSegments",True): # generated from line 90, col 3
            _v = VFFSL(SL,"segment.initialise",True) # '$segment.initialise' on line 91, col 1
            if _v is not None: write(_filter(_v, rawExpr='$segment.initialise')) # from line 91, col 1.
        # 
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    @callOncePerInstanceGuard
    def finalise(self, **KWS):



        ## CHEETAH: generated from @def finalise at line 97, col 1.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        # 
        _v = super(SequenceSegment, self).finalise()
        if _v is not None: write(_filter(_v))
        # 
        for segment in VFFSL(SL,"childSegments",True): # generated from line 101, col 3
            _v = VFFSL(SL,"segment.finalise",True) # '$segment.finalise' on line 102, col 1
            if _v is not None: write(_filter(_v, rawExpr='$segment.finalise')) # from line 102, col 1.
        # 
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def writeBody(self, **KWS):



        ## CHEETAH: main method generated for this template
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        write('''
''')
        # 
        # SequenceSegment.tmpl
        # 
        # Created by Graham Dennis on 2008-03-18.
        # 
        # Copyright (c) 2008-2012, Graham Dennis
        # 
        # This program is free software: you can redistribute it and/or modify
        # it under the terms of the GNU General Public License as published by
        # the Free Software Foundation, either version 2 of the License, or
        # (at your option) any later version.
        # 
        # This program is distributed in the hope that it will be useful,
        # but WITHOUT ANY WARRANTY; without even the implied warranty of
        # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        # GNU General Public License for more details.
        # 
        # You should have received a copy of the GNU General Public License
        # along with this program.  If not, see <http://www.gnu.org/licenses/>.
        # 
        write('''
''')
        # 
        #   Description of template
        write('''






''')
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        
    ##################################################
    ## CHEETAH GENERATED ATTRIBUTES


    _CHEETAH__instanceInitialized = False

    _CHEETAH_version = __CHEETAH_version__

    _CHEETAH_versionTuple = __CHEETAH_versionTuple__

    _CHEETAH_genTime = __CHEETAH_genTime__

    _CHEETAH_genTimestamp = __CHEETAH_genTimestamp__

    _CHEETAH_src = __CHEETAH_src__

    _CHEETAH_srcLastModified = __CHEETAH_srcLastModified__

    _mainCheetahMethod_for_SequenceSegment = 'writeBody'

## END CLASS DEFINITION

if not hasattr(SequenceSegment, '_initCheetahAttributes'):
    templateAPIClass = getattr(SequenceSegment,
                               '_CHEETAH_templateClass',
                               Template)
    templateAPIClass._addCheetahPlumbingCodeToClass(SequenceSegment)


# CHEETAH was developed by Tavis Rudd and Mike Orr
# with code, advice and input from many other volunteers.
# For more information visit https://cheetahtemplate.org/

##################################################
## if run from command line:
if __name__ == '__main__':
    from Cheetah.TemplateCmdLineIface import CmdLineIface
    CmdLineIface(templateObj=SequenceSegment()).run()


