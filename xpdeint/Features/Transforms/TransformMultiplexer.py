#!/usr/bin/env python3




##################################################
## DEPENDENCIES
import sys
import os
import os.path
try:
    import builtins as builtin
except ImportError:
    import __builtin__ as builtin
from os.path import getmtime, exists
import time
import types
from Cheetah.Version import MinCompatibleVersion as RequiredCheetahVersion
from Cheetah.Version import MinCompatibleVersionTuple as RequiredCheetahVersionTuple
from Cheetah.Template import Template
from Cheetah.DummyTransaction import *
from Cheetah.NameMapper import NotFound, valueForName, valueFromSearchList, valueFromFrameOrSearchList
from Cheetah.CacheRegion import CacheRegion
import Cheetah.Filters as Filters
import Cheetah.ErrorCatchers as ErrorCatchers
from Cheetah.compat import unicode
from xpdeint.Features.Transforms._TransformMultiplexer import _TransformMultiplexer

##################################################
## MODULE CONSTANTS
VFFSL=valueFromFrameOrSearchList
VFSL=valueFromSearchList
VFN=valueForName
currentTime=time.time
__CHEETAH_version__ = '3.2.6.post2'
__CHEETAH_versionTuple__ = (3, 2, 6, 'post', 2)
__CHEETAH_genTime__ = 1634954793.118098
__CHEETAH_genTimestamp__ = 'Sat Oct 23 13:06:33 2021'
__CHEETAH_src__ = '/home/mattias/xmds-3.0.0/admin/staging/xmds-3.1.0/xpdeint/Features/Transforms/TransformMultiplexer.tmpl'
__CHEETAH_srcLastModified__ = 'Thu Apr  4 16:29:24 2019'
__CHEETAH_docstring__ = 'Autogenerated by Cheetah: The Python-Powered Template Engine'

if __CHEETAH_versionTuple__ < RequiredCheetahVersionTuple:
    raise AssertionError(
      'This template was compiled with Cheetah version'
      ' %s. Templates compiled before version %s must be recompiled.'%(
         __CHEETAH_version__, RequiredCheetahVersion))

##################################################
## CLASSES

class TransformMultiplexer(_TransformMultiplexer):

    ##################################################
    ## CHEETAH GENERATED METHODS


    def __init__(self, *args, **KWs):

        super(TransformMultiplexer, self).__init__(*args, **KWs)
        if not self._CHEETAH__instanceInitialized:
            cheetahKWArgs = {}
            allowedKWs = 'searchList namespaces filter filtersLib errorCatcher'.split()
            for k,v in KWs.items():
                if k in allowedKWs: cheetahKWArgs[k] = v
            self._initCheetahInstance(**cheetahKWArgs)
        

    def description(self, **KWS):



        ## Generated from @def description: Transform Multiplexer at line 25, col 1.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        write('''Transform Multiplexer''')
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def includes(self, **KWS):



        ## CHEETAH: generated from @def includes at line 27, col 1.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        write('''#include <utility>
#include <map>
''')
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def globals(self, **KWS):



        ## CHEETAH: generated from @def globals at line 32, col 1.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        # 
        write('''typedef pair<ptrdiff_t, ptrdiff_t> _basis_pair;
typedef void (*transform_function)(bool, real, real* const __restrict__, real* const __restrict__, ptrdiff_t, ptrdiff_t);

// Less than operator needed by the C++ map class
struct _basis_pair_less_than
{
  bool operator()(const _basis_pair& _x, const _basis_pair& _y) const {
    return (_x.first < _y.first) || ((_x.first == _y.first) && (_x.second < _y.second));
  }
};

struct _transform_step
{
  transform_function _func;
  bool _forward;
  bool _out_of_place;
  ptrdiff_t _prefix_lattice;
  ptrdiff_t _postfix_lattice;
};

// Structure to hold the basis change information
struct _basis_transform_t
{
  vector<_transform_step> _transform_steps;
  real _multiplier;
  
  _basis_transform_t(real _multiplier_in = 1.0) : _multiplier(_multiplier_in) {}
  
  _basis_transform_t(const _basis_transform_t& _b) : _transform_steps(_b._transform_steps), _multiplier(_b._multiplier) {}
  
  void append(transform_function _func, bool _forward, bool _out_of_place, ptrdiff_t _prefix_lattice, ptrdiff_t _postfix_lattice)
  {
    _transform_steps.push_back((_transform_step){_func, _forward, _out_of_place, _prefix_lattice, _postfix_lattice});
  }
};

// Map type for holding (old_basis, new_basis) -> _basis_transform_t mappings
typedef map<_basis_pair, _basis_transform_t, _basis_pair_less_than> _basis_map;

''')
        for vector in self.vectorTransformMap: # generated from line 73, col 3
            write('''_basis_map _''')
            _v = VFFSL(SL,"vector.id",True) # '${vector.id}' on line 74, col 13
            if _v is not None: write(_filter(_v, rawExpr='${vector.id}')) # from line 74, col 13.
            write('''_basis_map;
''')
        write('''
real *_auxiliary_array = NULL;

const char *_basis_identifiers[] = {
''')
        for idx, basis in enumerate(self.basesNeeded): # generated from line 80, col 3
            write('''  /* ''')
            _v = VFFSL(SL,"idx",True) # '${idx}' on line 81, col 6
            if _v is not None: write(_filter(_v, rawExpr='${idx}')) # from line 81, col 6.
            write(''' */ "(''')
            _v = ', '.join(basis) # "${', '.join(basis)}" on line 81, col 18
            if _v is not None: write(_filter(_v, rawExpr="${', '.join(basis)}")) # from line 81, col 18.
            write(''')",
''')
        write('''};
''')
        # 
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def basisTransformFunctionContentsBegin(self, dict, **KWS):


        """
        Returns the ``basis_transform`` function implementation for vector `vector`.
        
        This writes the function that does the fourier transforming of a specific vector
        to and from arbitrary combinations of fourier-space and normal-space.
        """

        ## CHEETAH: generated from @def basisTransformFunctionContentsBegin($dict) at line 87, col 1.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        # 
        function = dict['function']
        vector = dict['caller']
        transformInfo = self.vectorTransformMap[vector]
        write('''if (_''')
        _v = VFFSL(SL,"vector.id",True) # '${vector.id}' on line 98, col 6
        if _v is not None: write(_filter(_v, rawExpr='${vector.id}')) # from line 98, col 6.
        write('''_basis == new_basis)
  return;

if (_''')
        _v = VFFSL(SL,"vector.id",True) # '${vector.id}' on line 101, col 6
        if _v is not None: write(_filter(_v, rawExpr='${vector.id}')) # from line 101, col 6.
        write('''_basis == -1) {
  _LOG(
    _ERROR_LOG_LEVEL,
    "Error: Attempted to transform the vector \'''')
        _v = VFFSL(SL,"vector.id",True) # '${vector.id}' on line 104, col 48
        if _v is not None: write(_filter(_v, rawExpr='${vector.id}')) # from line 104, col 48.
        write('''\' to basis %s, but the vector doesn\'t have a basis specified yet!\\n"
    "       Please report this error to ''')
        _v = VFFSL(SL,"bugReportAddress",True) # '$bugReportAddress' on line 105, col 41
        if _v is not None: write(_filter(_v, rawExpr='$bugReportAddress')) # from line 105, col 41.
        write('''\\n",
    _basis_identifiers[new_basis]
    );
}

if (_''')
        _v = VFFSL(SL,"vector.id",True) # '${vector.id}' on line 110, col 6
        if _v is not None: write(_filter(_v, rawExpr='${vector.id}')) # from line 110, col 6.
        write('''_basis_map.count(_basis_pair(_''')
        _v = VFFSL(SL,"vector.id",True) # '${vector.id}' on line 110, col 48
        if _v is not None: write(_filter(_v, rawExpr='${vector.id}')) # from line 110, col 48.
        write('''_basis, new_basis)) == 0) {
  _LOG(
    _ERROR_LOG_LEVEL,
    "Error: We should have information about how to do every needed transform, but it seems we don\'t for this transform.\\n"
    "       The transform is for the vector \'''')
        _v = VFFSL(SL,"vector.id",True) # '${vector.id}' on line 114, col 46
        if _v is not None: write(_filter(_v, rawExpr='${vector.id}')) # from line 114, col 46.
        write('''\' from basis %s to basis %s.\\n",
    _basis_identifiers[_''')
        _v = VFFSL(SL,"vector.id",True) # '${vector.id}' on line 115, col 25
        if _v is not None: write(_filter(_v, rawExpr='${vector.id}')) # from line 115, col 25.
        write('''_basis], _basis_identifiers[new_basis]
  );
}
_basis_transform_t &_t = _''')
        _v = VFFSL(SL,"vector.id",True) # '${vector.id}' on line 118, col 27
        if _v is not None: write(_filter(_v, rawExpr='${vector.id}')) # from line 118, col 27.
        write('''_basis_map[_basis_pair(_''')
        _v = VFFSL(SL,"vector.id",True) # '${vector.id}' on line 118, col 63
        if _v is not None: write(_filter(_v, rawExpr='${vector.id}')) # from line 118, col 63.
        write('''_basis, new_basis)];
if (_t._transform_steps.size() == 0) {
  _LOG(_ERROR_LOG_LEVEL, "Error: It looks like we tried to create plans for this transform, but failed.\\n"
                         "       The transform was for the vector \'''')
        _v = VFFSL(SL,"vector.id",True) # '${vector.id}' on line 121, col 68
        if _v is not None: write(_filter(_v, rawExpr='${vector.id}')) # from line 121, col 68.
        write('''\' from basis %s to basis %s.\\n",
                         _basis_identifiers[_''')
        _v = VFFSL(SL,"vector.id",True) # '${vector.id}' on line 122, col 46
        if _v is not None: write(_filter(_v, rawExpr='${vector.id}')) # from line 122, col 46.
        write('''_basis], _basis_identifiers[new_basis]);
}
real *_source_data = reinterpret_cast<real*>(_active_''')
        _v = VFFSL(SL,"vector.id",True) # '${vector.id}' on line 124, col 54
        if _v is not None: write(_filter(_v, rawExpr='${vector.id}')) # from line 124, col 54.
        write(''');
real *_dest_data = _auxiliary_array;
for (vector<_transform_step>::iterator _it = _t._transform_steps.begin(); _it != _t._transform_steps.end(); ++_it) {
  _it->_func(_it->_forward, _t._multiplier, _source_data, _dest_data, _it->_prefix_lattice, _it->_postfix_lattice);
  if (_it->_out_of_place) {
    real *_temp = _source_data;
    _source_data = _dest_data;
    _dest_data = _temp;
  }
}
_''')
        _v = VFFSL(SL,"vector.id",True) # '${vector.id}' on line 134, col 2
        if _v is not None: write(_filter(_v, rawExpr='${vector.id}')) # from line 134, col 2.
        write('''_basis = new_basis;
''')
        # 
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def oopCopyTransformFunction(self, transformID, transformDict, function, **KWS):



        ## CHEETAH: generated from @def oopCopyTransformFunction(transformID, transformDict, function) at line 138, col 1.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        # 
        write('''memcpy(_data_out, _data_in, _prefix_lattice * _postfix_lattice * sizeof(real));
''')
        # 
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def ipMultiplyTransformFunction(self, transformID, transformDict, function, **KWS):



        ## CHEETAH: generated from @def ipMultiplyTransformFunction(transformID, transformDict, function) at line 144, col 1.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        # 
        featureOrdering = ['OpenMP']
        featureDict = {    'templateString': ''  }
        write('''#pragma ivdep
''')
        _v = VFFSL(SL,"insertCodeForFeatures",False)('loopOverVectorsWithInnerContentTemplateBegin', featureOrdering, featureDict) # "${insertCodeForFeatures('loopOverVectorsWithInnerContentTemplateBegin', featureOrdering, featureDict)}" on line 151, col 1
        if _v is not None: write(_filter(_v, rawExpr="${insertCodeForFeatures('loopOverVectorsWithInnerContentTemplateBegin', featureOrdering, featureDict)}")) # from line 151, col 1.
        write('''for (long _i0 = 0; _i0 < _prefix_lattice * _postfix_lattice; _i0++) {
  _data_in[_i0] *= _multiplier;
}
''')
        _v = VFFSL(SL,"insertCodeForFeaturesInReverseOrder",False)('loopOverVectorsWithInnerContentTemplateEnd', featureOrdering, featureDict) # "${insertCodeForFeaturesInReverseOrder('loopOverVectorsWithInnerContentTemplateEnd', featureOrdering, featureDict)}" on line 155, col 1
        if _v is not None: write(_filter(_v, rawExpr="${insertCodeForFeaturesInReverseOrder('loopOverVectorsWithInnerContentTemplateEnd', featureOrdering, featureDict)}")) # from line 155, col 1.
        # 
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def oopMultiplyTransformFunction(self, transformID, transformDict, function, **KWS):



        ## CHEETAH: generated from @def oopMultiplyTransformFunction(transformID, transformDict, function) at line 159, col 1.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        # 
        featureOrdering = ['OpenMP']
        featureDict = {    'templateString': ''  }
        write('''#pragma ivdep
''')
        _v = VFFSL(SL,"insertCodeForFeatures",False)('loopOverVectorsWithInnerContentTemplateBegin', featureOrdering, featureDict) # "${insertCodeForFeatures('loopOverVectorsWithInnerContentTemplateBegin', featureOrdering, featureDict)}" on line 166, col 1
        if _v is not None: write(_filter(_v, rawExpr="${insertCodeForFeatures('loopOverVectorsWithInnerContentTemplateBegin', featureOrdering, featureDict)}")) # from line 166, col 1.
        write('''for (long _i0 = 0; _i0 < _prefix_lattice * _postfix_lattice; _i0++) {
  _data_out[_i0] = _data_in[_i0] * _multiplier;
}
''')
        _v = VFFSL(SL,"insertCodeForFeaturesInReverseOrder",False)('loopOverVectorsWithInnerContentTemplateEnd', featureOrdering, featureDict) # "${insertCodeForFeaturesInReverseOrder('loopOverVectorsWithInnerContentTemplateEnd', featureOrdering, featureDict)}" on line 170, col 1
        if _v is not None: write(_filter(_v, rawExpr="${insertCodeForFeaturesInReverseOrder('loopOverVectorsWithInnerContentTemplateEnd', featureOrdering, featureDict)}")) # from line 170, col 1.
        # 
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def mainBegin(self, dict, **KWS):



        ## CHEETAH: generated from @def mainBegin($dict) at line 175, col 1.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        # 
        write('''
_basis_transform_t *_basis_transform = NULL;
ptrdiff_t _auxiliary_array_size = 0;
ptrdiff_t _max_vector_size = 0;
real* _max_vector_array = NULL;

''')
        boolMap = {True: 'true', False: 'false'}
        for vector, vectorTransformInfo in self.vectorTransformMap.items(): # generated from line 184, col 3
            sizePrefix = '2 * ' if vector.type == 'complex' else ''
            write('''if (''')
            _v = VFFSL(SL,"sizePrefix",True) # '${sizePrefix}' on line 186, col 5
            if _v is not None: write(_filter(_v, rawExpr='${sizePrefix}')) # from line 186, col 5.
            _v = VFFSL(SL,"vector.allocSize",True) # '${vector.allocSize}' on line 186, col 18
            if _v is not None: write(_filter(_v, rawExpr='${vector.allocSize}')) # from line 186, col 18.
            write(''' > _max_vector_size) {
  _max_vector_size = ''')
            _v = VFFSL(SL,"sizePrefix",True) # '${sizePrefix}' on line 187, col 22
            if _v is not None: write(_filter(_v, rawExpr='${sizePrefix}')) # from line 187, col 22.
            _v = VFFSL(SL,"vector.allocSize",True) # '${vector.allocSize}' on line 187, col 35
            if _v is not None: write(_filter(_v, rawExpr='${vector.allocSize}')) # from line 187, col 35.
            write(''';
  _max_vector_array = reinterpret_cast<real*>(_''')
            _v = VFFSL(SL,"vector.id",True) # '${vector.id}' on line 188, col 48
            if _v is not None: write(_filter(_v, rawExpr='${vector.id}')) # from line 188, col 48.
            write(''');
}
''')
            needsAuxiliaryArray = False
            bases = vectorTransformInfo['bases']
            basisPairMap = vectorTransformInfo['basisPairMap']
            for basisPairInfo in basisPairMap.values(): # generated from line 193, col 5
                basisPair = basisPairInfo['basisPair']
                write('''_basis_transform = &_''')
                _v = VFFSL(SL,"vector.id",True) # '${vector.id}' on line 195, col 22
                if _v is not None: write(_filter(_v, rawExpr='${vector.id}')) # from line 195, col 22.
                write('''_basis_map[_basis_pair(''')
                _v = ', '.join([str(self.basisIndexForBasis(basis)) for basis in basisPair]) # "${', '.join([str(self.basisIndexForBasis(basis)) for basis in basisPair])}" on line 195, col 57
                if _v is not None: write(_filter(_v, rawExpr="${', '.join([str(self.basisIndexForBasis(basis)) for basis in basisPair])}")) # from line 195, col 57.
                write(''')];
''')
                if basisPairInfo['forwardScale']: # generated from line 196, col 7
                    write('''_basis_transform->_multiplier = ''')
                    _v = ' * '.join(basisPairInfo['forwardScale']) # "${' * '.join(basisPairInfo['forwardScale'])}" on line 197, col 33
                    if _v is not None: write(_filter(_v, rawExpr="${' * '.join(basisPairInfo['forwardScale'])}")) # from line 197, col 33.
                    write(''';
''')
                for transformStep in basisPairInfo['transformSteps']: # generated from line 199, col 7
                    needsAuxiliaryArray = True if transformStep[2] else needsAuxiliaryArray
                    write('''_basis_transform->append(
  /* transform function */ _transform_''')
                    _v = VFFSL(SL,"transformStep",True)[0] # '${transformStep[0]}' on line 202, col 39
                    if _v is not None: write(_filter(_v, rawExpr='${transformStep[0]}')) # from line 202, col 39.
                    write(''',
  /* forward? */ ''')
                    _v = VFFSL(SL,"boolMap",True)[transformStep[1]] # '${boolMap[transformStep[1]]}' on line 203, col 18
                    if _v is not None: write(_filter(_v, rawExpr='${boolMap[transformStep[1]]}')) # from line 203, col 18.
                    write(''',
  /* out-of-place? */ ''')
                    _v = VFFSL(SL,"boolMap",True)[transformStep[2]] # '${boolMap[transformStep[2]]}' on line 204, col 23
                    if _v is not None: write(_filter(_v, rawExpr='${boolMap[transformStep[2]]}')) # from line 204, col 23.
                    write(''',
  /* prefix lattice */ ''')
                    _v = VFFSL(SL,"transformStep",True)[3] # '${transformStep[3]}' on line 205, col 24
                    if _v is not None: write(_filter(_v, rawExpr='${transformStep[3]}')) # from line 205, col 24.
                    write(''',
  /* postfix lattice*/ ''')
                    _v = VFFSL(SL,"transformStep",True)[4] # '${transformStep[4]}' on line 206, col 24
                    if _v is not None: write(_filter(_v, rawExpr='${transformStep[4]}')) # from line 206, col 24.
                    write('''
);
''')
                write('''
_basis_transform = &_''')
                _v = VFFSL(SL,"vector.id",True) # '${vector.id}' on line 210, col 22
                if _v is not None: write(_filter(_v, rawExpr='${vector.id}')) # from line 210, col 22.
                write('''_basis_map[_basis_pair(''')
                _v = ', '.join([str(self.basisIndexForBasis(basis)) for basis in reversed(basisPair)]) # "${', '.join([str(self.basisIndexForBasis(basis)) for basis in reversed(basisPair)])}" on line 210, col 57
                if _v is not None: write(_filter(_v, rawExpr="${', '.join([str(self.basisIndexForBasis(basis)) for basis in reversed(basisPair)])}")) # from line 210, col 57.
                write(''')];
''')
                if basisPairInfo['backwardScale']: # generated from line 211, col 7
                    write('''_basis_transform->_multiplier = ''')
                    _v = ' * '.join(basisPairInfo['backwardScale']) # "${' * '.join(basisPairInfo['backwardScale'])}" on line 212, col 33
                    if _v is not None: write(_filter(_v, rawExpr="${' * '.join(basisPairInfo['backwardScale'])}")) # from line 212, col 33.
                    write(''';
''')
                for transformStep in reversed(basisPairInfo['transformSteps']): # generated from line 214, col 7
                    needsAuxiliaryArray = True if transformStep[2] else needsAuxiliaryArray
                    write('''_basis_transform->append(
  /* transform function */ _transform_''')
                    _v = VFFSL(SL,"transformStep",True)[0] # '${transformStep[0]}' on line 217, col 39
                    if _v is not None: write(_filter(_v, rawExpr='${transformStep[0]}')) # from line 217, col 39.
                    write(''',
  /* forward? */ ''')
                    _v = VFFSL(SL,"boolMap",True)[not transformStep[1]] # '${boolMap[not transformStep[1]]}' on line 218, col 18
                    if _v is not None: write(_filter(_v, rawExpr='${boolMap[not transformStep[1]]}')) # from line 218, col 18.
                    write(''',
  /* out-of-place? */ ''')
                    _v = VFFSL(SL,"boolMap",True)[transformStep[2]] # '${boolMap[transformStep[2]]}' on line 219, col 23
                    if _v is not None: write(_filter(_v, rawExpr='${boolMap[transformStep[2]]}')) # from line 219, col 23.
                    write(''',
  /* prefix lattice */ ''')
                    _v = VFFSL(SL,"transformStep",True)[3] # '${transformStep[3]}' on line 220, col 24
                    if _v is not None: write(_filter(_v, rawExpr='${transformStep[3]}')) # from line 220, col 24.
                    write(''',
  /* postfix lattice */ ''')
                    _v = VFFSL(SL,"transformStep",True)[4] # '${transformStep[4]}' on line 221, col 25
                    if _v is not None: write(_filter(_v, rawExpr='${transformStep[4]}')) # from line 221, col 25.
                    write('''
);
''')
                write('''
''')
            if needsAuxiliaryArray: # generated from line 226, col 5
                sizePrefix = '2 * ' if vector.type == 'complex' else ''
                write('''_auxiliary_array_size = MAX(_auxiliary_array_size, ''')
                _v = VFFSL(SL,"sizePrefix",True) # '${sizePrefix}' on line 228, col 52
                if _v is not None: write(_filter(_v, rawExpr='${sizePrefix}')) # from line 228, col 52.
                _v = VFFSL(SL,"vector.allocSize",True) # '${vector.allocSize}' on line 228, col 65
                if _v is not None: write(_filter(_v, rawExpr='${vector.allocSize}')) # from line 228, col 65.
                write("""); // vector '""")
                _v = VFFSL(SL,"vector.name",True) # '${vector.name}' on line 228, col 98
                if _v is not None: write(_filter(_v, rawExpr='${vector.name}')) # from line 228, col 98.
                write("""' needs an out-of-place transform

""")
        write('''if (_auxiliary_array_size) {
  _auxiliary_array = (real*) xmds_malloc(sizeof(real) * _auxiliary_array_size);
}

bool _allocated_temporary_array = false;
if (!_max_vector_array && _max_vector_size > 0) {
  _max_vector_array = (real*) xmds_malloc(sizeof(real) * _max_vector_size);
  _allocated_temporary_array = true;
}

// Make all geometry-dependent transformations prepare plans, etc.
''')
        for tID, transformation in enumerate(self.neededTransformations): # generated from line 243, col 3
            if not transformation.get('geometryDependent', False): # generated from line 244, col 5
                continue
            write('''_transform_''')
            _v = VFFSL(SL,"tID",True) # '${tID}' on line 247, col 12
            if _v is not None: write(_filter(_v, rawExpr='${tID}')) # from line 247, col 12.
            write('''(true, 1.0, _max_vector_array, _auxiliary_array, ''')
            _v = VFFSL(SL,"transformation",True)['prefixLatticeString'] # "${transformation['prefixLatticeString']}" on line 247, col 67
            if _v is not None: write(_filter(_v, rawExpr="${transformation['prefixLatticeString']}")) # from line 247, col 67.
            write(''', ''')
            _v = VFFSL(SL,"transformation",True)['postfixLatticeString'] # "${transformation['postfixLatticeString']}" on line 247, col 109
            if _v is not None: write(_filter(_v, rawExpr="${transformation['postfixLatticeString']}")) # from line 247, col 109.
            write(''');
''')
        write('''
if (_allocated_temporary_array) {
  xmds_free(_max_vector_array);
}
''')
        # 
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def mainEnd(self, dict, **KWS):



        ## CHEETAH: generated from @def mainEnd($dict) at line 256, col 1.
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        # 
        write('''if (_auxiliary_array) {
  xmds_free(_auxiliary_array);
}
''')
        # 
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        

    def writeBody(self, **KWS):



        ## CHEETAH: main method generated for this template
        trans = KWS.get("trans")
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        write('''
''')
        # 
        # TransformMultiplexer.tmpl
        # 
        # Created by Graham Dennis on 2008-12-23.
        # 
        # Copyright (c) 2008-2012, Graham Dennis
        # 
        # This program is free software: you can redistribute it and/or modify
        # it under the terms of the GNU General Public License as published by
        # the Free Software Foundation, either version 2 of the License, or
        # (at your option) any later version.
        # 
        # This program is distributed in the hope that it will be useful,
        # but WITHOUT ANY WARRANTY; without even the implied warranty of
        # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        # GNU General Public License for more details.
        # 
        # You should have received a copy of the GNU General Public License
        # along with this program.  If not, see <http://www.gnu.org/licenses/>.
        # 
        write('''








''')
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        
    ##################################################
    ## CHEETAH GENERATED ATTRIBUTES


    _CHEETAH__instanceInitialized = False

    _CHEETAH_version = __CHEETAH_version__

    _CHEETAH_versionTuple = __CHEETAH_versionTuple__

    _CHEETAH_genTime = __CHEETAH_genTime__

    _CHEETAH_genTimestamp = __CHEETAH_genTimestamp__

    _CHEETAH_src = __CHEETAH_src__

    _CHEETAH_srcLastModified = __CHEETAH_srcLastModified__

    _mainCheetahMethod_for_TransformMultiplexer = 'writeBody'

## END CLASS DEFINITION

if not hasattr(TransformMultiplexer, '_initCheetahAttributes'):
    templateAPIClass = getattr(TransformMultiplexer,
                               '_CHEETAH_templateClass',
                               Template)
    templateAPIClass._addCheetahPlumbingCodeToClass(TransformMultiplexer)


# CHEETAH was developed by Tavis Rudd and Mike Orr
# with code, advice and input from many other volunteers.
# For more information visit https://cheetahtemplate.org/

##################################################
## if run from command line:
if __name__ == '__main__':
    from Cheetah.TemplateCmdLineIface import CmdLineIface
    CmdLineIface(templateObj=TransformMultiplexer()).run()


