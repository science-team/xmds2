.. index:: Installing XMDS2

.. _Installation:

************
Installation
************

**XMDS2** can be installed on any Unix-like system including Linux and Mac OS X.  It requires a C++ compiler, python, and several installed packages.  Many of these packages are optional, but a good idea to obtain full functionality. Microsoft Windows is not officially supported at this time, but people have reported success in installing it under Windows 10 using the Windows Subsystem for Linux. 

Overview
========

If you're using a Mac with OS X >= 10.7, XMDS2 can be installed with the Homebrew package manager.

If you are using a recent version of Ubuntu or Debian, XMDS2 is available as a package in your package / software manager. Note that if using the Ubuntu Software Centre rather than Synaptic, you must explicitly search for "xmds2" rather than "xmds", or you will only get XMDS1 as a result.

If you're using a version of Linux that's not Ubuntu / Debian, the easiest way to get started is to use the install shell script linked to below. 

If we don't have an installer for your system, or you would like more control over the install, follow the :ref:`manual installation <ManualInstallation>` instructions.

.. tabularcolumns:: |c|c|

.. list-table::
    :widths: 20, 15
    :header-rows: 0

    * - Linux (Ubuntu/Debian/Fedora/RedHat)

      - :ref:`Linux install <linux_installation>`

    * - Mac OS X

      - :ref:`Mac OS X install <mac_installation>`
        
    * - Other systems

      - :ref:`Manual install <ManualInstallation>`


If you have one of the supported operating systems listed above, but you find the installer doesn't work for you, please let us know by emailing xmds-devel <at> lists.sourceforge.net. If you'd like to tweak the linux installer to work on a distribution we haven't tested, we'd love you to do that and let us know!

Once you have it installed, see the :ref:`QuickStartTutorial` for next steps. In addition, feel free to browse the extensive examples that come with XMDS2. Depending on the installation method, you can find them in the examples/ folder, or if that didn't ship with your installer you can find them `here <https://sourceforge.net/p/xmds/code/HEAD/tree/trunk/xpdeint/examples/>`_.


.. index:: Installers - linux

.. _linux_installation:

Linux installation instructions
===============================

Note: If you're using Debian / Ubuntu, unless you want a developer install, you're probably better off installing XMDS2 via your package manager rather than this install script.

The linux installer has currently only been tested with Ubuntu, Debian, Fedora, and Red Hat. If you are using Red Hat you must have the EPEL repository enabled.

Download the installer here: http://svn.code.sf.net/p/xmds/code/trunk/xpdeint/admin/linux_installer.sh

Once you have downloaded it, make the installer executable and run it by typing the following into a terminal::

  chmod u+x linux_installer.sh
  ./linux_installer.sh


The linux installer installs all XMDS2 dependencies from your native package manager where possible (``apt-get`` for Ubuntu/Debian, ``yum`` for Fedora/Red Hat) but will download and compile the source code for libraries not available through the package manager. It also downloads the XMDS2 code itself. This means you'll need to be connected to the internet when running the installer. The installer should not be run with administrative privileges; it will ask you to enter your admin password at the appropriate point. 

For instructions on how to install XMDS2 on systems where you lack administrative rights, see :ref:`ManualInstallation`.

By default, this installer will install a known stable version of XMDS, which can be updated at any time by navigating to the XMDS directory and typing 'make update'. To install the latest developer version at the beginning, simply run the installer with the ``--develop`` option.

Once XMDS2 has been installed, you can run it from the terminal by typing ``xmds2``.


.. index:: Installers - Max OS X

.. _mac_installation:

Mac OS X installation
=====================

You will need to install the `Homebrew package manager <https://brew.sh/>`_

You will also need Xcode (Apple's developer tools), which can be downloaded free of charge from the App store.

Once you have Homebrew and Xcode, run the following in a terminal::

  brew install xmds2/xmds2/xmds2

This will download and install everything required for XMDS2. Note that this may take a significant amount of time, as many packages need to be downloaded, and some libraries may need to be compiled locally on your machine. To uninstall XMDS2, run the following::

  brew uninstall xmds2

Once XMDS2 has been installed, you can run it from the terminal by typing ``xmds2``.

Note that this installer does not install some python libraries used in XMDS2 development. If you wish to run the testsuite on your machine (not required for ordinary users), you will have to additionally install Python 3 versions of pyparsing, h5py and numpy. You can do this via ``pip3 install pyparsing h5py numpy``.


.. index:: Installing via source

.. _ManualInstallation:

Manual installation from source
===============================

This installation guide will take you through a typical full install step by step. A large part of this procedure is obtaining and installing other libraries that XMDS2 requires, before installing XMDS2 itself. 

While the instructions below detail these packages individually, if you have administrative privileges (or can request packages from your administrator) and if you are using an Ubuntu, Debian, Fedora or Red Hat linux distribution, you can install all required and optional dependencies (but not XMDS2 itself), including those needed to build the documenation, via

Ubuntu / Debian::

  sudo apt-get -y install build-essential subversion libfftw3-dev libfftw3-mpi-dev libopenmpi-dev openmpi-bin python3-dev python3-setuptools python3-numpy python3-pyparsing python3-lxml python3-mpmath libhdf5-serial-dev libgsl-dev python3-sphinx python3-h5py wget libatlas-base-dev python3-pip

Fedora / Red Hat::

  sudo yum -y install gcc gcc-c++ blas-devel sqlite-devel make automake fftw-devel fftw-openmpi-devel gsl-devel hdf5-devel openmpi-devel python3-devel python3-h5py python3-lxml python3-mpmath python3-numpy python3-pyparsing python3-setuptools python3-sphinx python3-cheetah wget libxml2-devel libxslt-devel subversion


Also note that this guide adds extra notes for users wishing to install XMDS2 using the SVN repository.  This requires a few extra steps, but allows you to edit your copy, and/or update your copy very efficiently (with all the usual advantages and disadvantages of using unreleased material).

0. You will need a copy of XMDS2.  
    The current release can be found at `Sourceforge <http://sourceforge.net/projects/xmds/>`_, and downloaded as a single file.
    Download this file, and expand it in a directory where you want to keep the program files.
    
    * Developer-only instructions: You can instead check out a working copy of the source using SVN. 
      In a directory where you want to check out the repository, run:
      ``svn checkout https://svn.code.sf.net/p/xmds/code/trunk/xpdeint .``
      (Only do this once.  To update your copy, type ``make update`` in the same directory, and then repeat any developer-only instructions below).
      A checkout with read/write permissions requires a checkout with your login details.  e.g.:
      ``svn checkout --username=myusername https://myusername@svn.code.sf.net/p/xmds/code/trunk/xpdeint .``
    
#. You will need a working C++ compiler.  
    For Mac OS X, this means that the developer tools (XCode) should be installed. We also recommend you install the Xcode command line tools to gain access to Apple's Accelerate framework and vecLib.
    One common free compiler is `gcc <http://gcc.gnu.org/>`_.  It can be downloaded using your favourite package manager.
    XMDS2 can also use Intel's C++ compiler if you have it.

#. You will need a `Python 3 distribution <http://www.python.org/>`_.

    XMDS2 is written in Python 3. This will be almost certainly be preinstalled in a Linux distribution, but Macs only ship with Python 2.

    * Mac OS X: Either install using something like the `Homebrew package manager <https://brew.sh/>`_ with ``brew install python3`` (recommended) or directly from www.python.org
    * Linux: It should be pre-installed. If not, install using your favourite package manager.
   

#. Install setuptools.

   This will almost certainly have been installed by your Python 3 distribution. If not, you can install with the Python 3 package manager (pip3) via ``pip3 install setuptools``


#. Install HDF5 and FFTW3 (and optionally MPI). You may choose to build specific versions optimized for your machine, and install them somewhere under your home directory structure rather than in system library locations, by passing "--prefix=/home/myname/path/to/directory" as an argument to the ./configure command. If you do this, after installing XMDS2 you will have to tell it where to find these specific libraries by running ``xmds2 --reconfigure`` as described below.
    .. _hdf5_Installation:
    
    #. **HDF5** is a library for reading and writing the `Hierarchical Data Format <http://www.hdfgroup.org/HDF5/>`_.
         This is a standardised data format which it is suggested that people use in preference to the older 'binary' output (which is 
         compatible with xmds-1). The advantage of HDF5 is that this data format is understood by a variety of other tools. xsil2graphics2
         provides support for loading data created in this format into Mathematica and Matlab.
         
         XMDS2 only requires the single process version of HDF5, so there is no need to install the MPI version.
       
         \* Sidebar: Installing HDF5 from source follows a common pattern, which you may find yourself repeating later:  
         
            #. After extracting the source directory, type ``./configure`` and then add possible options.
            
                (For HDF5, install with the ``--prefix=/usr/local/`` option if you want XMDS2 to find the library automatically.  This is rarely needed for other packages.)
                
            #. Once that is finished, type ``make``.  Then wait for that to finish, which will often be longer than you think.
            
            #. Finally, type ``sudo make install`` to install it into the appropriate directory.
        
    #. **FFTW** is the library XMDS2 uses for Fourier transforms. 
         This is the transform most people will use in their simulations. If you need
         support for MPI distributed simulations, you must configure FFTW to use MPI.
  
         FFTW is available for free at the `FFTW website <http://www.fftw.org/>`_.
         
         A reasonable configure command would be::

           ./configure --disable-fortran --enable-mpi --enable-sse2 --enable-openmp --enable-avx --enable-avx2 --enable-threads
           make
           sudo make install
         

         If you want to be able to use the single precision option within XMDS2, you must additionally build the single precision FFTW libraries via::

           ./configure --disable-fortran --enable-mpi --enable-single --enable-sse2 --enable-openmp --enable-avx --enable-avx2 --enable-threads
           make
           sudo make install

         Note that compiling OpenMP directives is difficult with a Mac as the Clang compiler doesn't support OpenMP at this time (although MPI works fine), so you'll probably want to omit the ``--enable-openmp`` flag. If you're on a Mac and really want OpenMP functionality in XMDS2 (unlikely, as for most XMDS2 use cases MPI is faster), build FFTW with gcc, then tell XMDS2 to use gcc over clang in general use. 

    #. **MPI** is an API for doing parallel processing.
         XMDS2 can use MPI to parallelise simulations on multi-processor/multi-core computers, or clusters of computers. Highly recommended.
         Many supercomputing systems come with MPI libraries pre-installed.
         The `Open MPI <http://www.open-mpi.org/>`_ project has free distributions of this library available.
		 
	 If you intend to take advantage of XMDS2's multi-processing features, you must install MPI, and build FFTW3 with the ``--enable-mpi`` flag in order to use it.



#. There are a range of optional installs.  We recommend that you install them all if possible:

    #. A Matrix library like `ATLAS <http://math-atlas.sourceforge.net/>`_, Intel's `MKL <http://software.intel.com/en-us/intel-mkl/>`_ or the `GNU Scientific library (GSL) <http://www.gnu.org/software/gsl/>`_ 
         These libraries allow efficient implementation of transform spaces other than Fourier space.
         Mac OS X comes with its own matrix library (vecLib) if you install Xcode and the Xcode command line tools.
         You may wish to build the libraries specifically tuned and optimized for your machine. Instructions for doing so for the ATLAS project can be found on their `website <http://math-atlas.sourceforge.net/>`_.
    
    #. **numpy** is a python tool that XMDS2 uses for matrix transforms and running the testsuite.
         It can be installed with ``pip3 install numpy``.
         
    #. **lxml** is used to validate the syntax of scripts passed to XMDS2. 
         This can be installed with the command ``pip3 install lxml``  

    #. **h5py** and **pyparsing** are needed for running the developer testsuite.
         To install use ``pip3 install h5py``
 

#. Install XMDS2 into your python path by running (in the xmds-3.1.0/ directory):
    ``sudo ./setup.py develop``

    If you want to install it into your home directory, type ``./setup.py develop --prefix=~``
    
    This step requires access to the internet, as it downloads any dependent packages.

    * Developer only instructions: 
        The Cheetah templates (\*.tmpl) must be compiled into python.
        To do this, run ``make`` in the xmds-3.1.0/ directory.

    * Developer-only instructions: 
        If you have 'numpy', 'pyparsing' and 'h5py' installed (see above), test XMDS2 by typing ``./run_tests.py`` in the xmds-3.1.0/ directory.
       
    * Developer-only instructions: 
        To build the user documentation, you first need to install Sphinx, either via your package manager or:
        ``sudo easy_install Sphinx``

        Then, to build the documentation, in the xmds-3.1.0/admin/userdoc-source/ directory run: ``make html``

        If this results in an error, you may need to run ``sudo ./setup.py develop``

        The generated html documentation will then be found at xmds-3.1.0/documentation/index.html
		
#. Configure XMDS2 by typing ``xmds2 --reconfigure``.  If XMDS2 is unable to find a library, you can tell XMDS2 where these libraries are located by adding ``include`` and ``lib`` search paths using the ``--include-path`` and ``--lib-path`` options.  For example, if FFTW3 is installed in ``/apps/fftw3`` with headers in ``/apps/fftw3/include/`` and the libraries in ``/apps/fftw3/lib``, (re)configure XMDS2 by typing:

	* ``xmds2 --reconfigure --include-path /apps/fftw3/include --lib-path /apps/fftw3/lib``.
	
	If you need to use additional compiler or link flags for XMDS2 to use certain libraries, set the ``CXXFLAGS`` or ``LINKFLAGS`` environment variables before calling ``xmds2 --reconfigure``.  For example, to pass the compiler flag ``-pedantic`` and the link flag ``-lm``, use:
	
	* ``CXXFLAGS="-pedantic" LINKFLAGS="-lm" xmds2 --reconfigure``.

**Congratulations!** You should now have a fully operational copy of xmds2 and xsil2graphics2.  You can test your copy using examples from the "xmds-3.1.0/examples" directory, and follow the worked examples in the :ref:`QuickStartTutorial` and :ref:`WorkedExamples`.



